// console.log('Hello')

// Arithmetic Operators
/* 
    + sum
    - subtraction
    * multiplication
    / division
    % modulo (returns the remainder)

*/

let x = 45;
let y = 28;

let sum = x + y;
console.log('Result of addition: ' + sum);

let difference = x - y;
console.log('Result of subtraction: ' + difference);

let product = x * y;
console.log('Result of multiplication: ' + product);

let quotient = x / y;
console.log('Result of division: ' + quotient);

let mod = x % y;
console.log('Result of modulo: ' + mod);

//Assignment operator
//Basic Assignment operator (=)

let assignmentNumber = 8;

//Arithmetic assignment operator
//Addition Assignment Operator (+=)
//Subtraction Assignment Operator (-=)
//Multiplication Assignment Operator (*=)
//Division Assignment Operator (/=)
//Modulo Assignment Operator (%=)

assignmentNumber = assignmentNumber + 2;
console.log('Result of addition operator: ' + assignmentNumber)


assignmentNumber += 2;

//PEMDAS
/* 

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log('Result of mdas: ' + mdas)

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log('Result of pemdas: ' + pemdas)


//Increment and Decrement
    /* Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
     */

let z = 1;
let increment = ++z; // z = z+1
/* the value "z" is added by a value of one before returning the value and storing it in variable "increment" 

*/
console.log("Pre-increment: " + increment);   

/* the value of "z" was laos increased even though we didn't implicitly specify any value reassignment 
 */
console.log("Value of z: " + z);


/* the value of z is returned and sored in the variable increment then th value of z is increased by 1 */
increment = z++;
//increment = 2 plus 1 (z+1)
console.log("Post-increment: " + increment);

//the value of z was increased again reassigning the value to 3
console.log("Value of z: " + z);

/* the value of z is decreased by a value of one before returning the value and storing it in the variable decrement */
let decrement = --z;
//the value of z is at 3 bfore it was decrement
console.log("Pre-decrement: " + decrement);
console.log("Value of z: " + z);


/* the value of z is returned and stored in the variable decrement then the value of z is decreased by 1 
 */
decrement = z--;
/* the value of z at 2 before it was decremented */
console.log("Post-decrement: " + decrement)

    

//Type of Coercion
/* 
is the automatic or implicit conversion of values from one data type to another
    - this happened when operations are performed on different data types that would normally not be possiable and yield irregular results
    - values are automatically converted from one data type to antoher in order to resolve operations
 */
let numA = '10';
let numB = 12;

/* 
    adding/concatenating a string and a number will result in a string
*/

let coercion = numA + numB;
console.log(coercion); //result: 1012
console.log(typeof(coercion));

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);

let numE = true + 1;
console.log(numE); //result: 1+1 =2
/* the boolean true is also associated with the value of 1 */

let numF = false + 2;
console.log(numF);


//Equality Operator

console.log(1 === 1);//true
console.log(1 == 2);//false
console.log(1 == '1');//true
console.log('jungkook' == 'jungkook'); //true
console.log('Jungkook' == 'jungkook'); //false
console.log('a' == 'A'); //false

//Strict Equality (===)
/* checks whether the operands are equal/have the same content
-also compare the data type of 2 values
-JS is a loosely typed language meaning that the values of different data tyoes can be stored in variables
- in combination with type coercion, this sometimes create problems within our code
-strict equality operators are better to use in most cases to ensure that data types provided are correct */
console.log(1 === 1); //true
console.log(1 === '1'); //false


//Inequality operator !=
/* checks whether the operands are not equal/have a different content */

console.log(1 != 1);//false
console.log(1 != 3);//true
console.log(1 != '1');//false
console.log('jungkook' != 'jungkook');//false
console.log('Jungkook' != 'jungkook');//true

//Strict inequality !==
/* checks whether the operands are not equal/have the same content
-also compares the data types of 2 values */
console.log(1 !== 1);//false
console.log(1 !== '1');//true

//Relational operator



//Logical operator










